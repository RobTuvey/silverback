use std::fmt::Debug;

pub trait Event: Debug {}
