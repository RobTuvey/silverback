use crate::Event;

pub trait EventHandler {
    fn handle_event(&mut self, event: &dyn Event);
}
