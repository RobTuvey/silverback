use crate::App;
use std::any::Any;

pub trait Module: Any + Send + Sync {
    fn init(&mut self, app: &mut App);
}