use crate::event::EventHandler;
use crate::Module;

use silverback_log::*;

pub struct App {
    modules: Vec<Box<dyn Module>>,
}

impl Default for App {
    fn default() -> Self {
        App::empty()
    }
}

impl App {
    pub fn new() -> App {
        LogModule::init();

        App::default()
    }

    pub fn add_module<T>(&mut self, module: T) -> &mut Self
    where
        T: Module,
    {
        let mut boxed = Box::new(module);
        boxed.init(self);
        self.modules.push(boxed);
        self
    }

    pub fn run(&mut self) {}

    fn empty() -> App {
        Self {
            modules: Vec::new(),
        }
    }
}

impl EventHandler for App {
    fn handle_event(&mut self, event: &dyn crate::Event) {
        sb_debug!("Handling event {event:?}");
    }
}
