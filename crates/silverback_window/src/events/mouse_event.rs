use std::fmt::Debug;

use silverback_app::Event;
use winit::event::MouseButton;

pub struct MouseEvent {
    event: MouseEventType,
}

#[derive(Debug)]
pub enum MouseEventType {
    Moved { x: f64, y: f64 },
    ButtonDown(MouseButton),
    ButtonUp(MouseButton),
}

impl MouseEvent {
    pub fn new(event: MouseEventType) -> MouseEvent {
        MouseEvent { event }
    }
}

impl Event for MouseEvent {}

impl Debug for MouseEvent {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("MouseEvent")
            .field("event", &self.event)
            .finish()
    }
}
