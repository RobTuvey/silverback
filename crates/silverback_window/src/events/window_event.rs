use std::fmt::Debug;

use silverback_app::Event;

pub struct WindowEvent {
    event: WindowEventType,
}

#[derive(Debug)]
pub enum WindowEventType {
    Closed,
}

impl WindowEvent {
    pub fn new(event: WindowEventType) -> WindowEvent {
        WindowEvent { event }
    }
}

impl Event for WindowEvent {}

impl Debug for WindowEvent {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("WindowEvent")
            .field("event", &format!("{:?}", self.event))
            .finish()
    }
}
