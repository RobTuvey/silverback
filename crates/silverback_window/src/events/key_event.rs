use std::fmt::Debug;

use silverback_app::Event;
use winit::keyboard::KeyCode;

pub struct KeyEvent {
    event: KeyEventType,
}

#[derive(Debug)]
pub enum KeyEventType {
    KeyUp(KeyCode),
    KeyDown(KeyCode),
}

impl KeyEvent {
    pub fn new(event: KeyEventType) -> KeyEvent {
        KeyEvent { event }
    }
}

impl Event for KeyEvent {}

impl Debug for KeyEvent {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("KeyEvent")
            .field("event", &self.event)
            .finish()
    }
}
