use winit::{
    event_loop::{ControlFlow, EventLoop},
    keyboard::PhysicalKey,
    window::WindowBuilder,
};

use silverback_log::*;

use silverback_app::{App, EventHandler, Module};

mod events;

pub use events::*;

pub use winit::event::MouseButton;
pub use winit::keyboard::KeyCode;

pub struct WindowModule;

impl WindowModule {
    pub fn new() -> WindowModule {
        WindowModule {}
    }
}

impl Module for WindowModule {
    fn init(&mut self, app: &mut App) {
        let event_loop = EventLoop::new().unwrap();
        let window = WindowBuilder::new().build(&event_loop).unwrap();

        event_loop.set_control_flow(ControlFlow::Poll);

        let _ = event_loop.run(move |event, elwt| match event {
            // Window closed
            winit::event::Event::WindowEvent {
                event: winit::event::WindowEvent::CloseRequested,
                ..
            } => {
                app.handle_event(&WindowEvent::new(WindowEventType::Closed));
                elwt.exit();
            }

            // Window about to wait
            winit::event::Event::AboutToWait => {
                window.request_redraw();
            }

            // Window redraw requested
            winit::event::Event::WindowEvent {
                event: winit::event::WindowEvent::RedrawRequested,
                ..
            } => {}

            // Key event
            winit::event::Event::DeviceEvent {
                event: winit::event::DeviceEvent::Key(key_event),
                ..
            } => {
                if let PhysicalKey::Code(key_code) = key_event.physical_key {
                    if key_event.state.is_pressed() {
                        app.handle_event(&KeyEvent::new(KeyEventType::KeyDown(key_code)));
                    } else {
                        app.handle_event(&KeyEvent::new(KeyEventType::KeyUp(key_code)));
                    }
                }
            }

            // Mouse moved
            winit::event::Event::WindowEvent {
                event: winit::event::WindowEvent::CursorMoved { position, .. },
                ..
            } => app.handle_event(&MouseEvent::new(MouseEventType::Moved {
                x: position.x,
                y: position.y,
            })),

            // Mouse button
            winit::event::Event::WindowEvent {
                event: winit::event::WindowEvent::MouseInput { state, button, .. },
                ..
            } => {
                if state.is_pressed() {
                    app.handle_event(&MouseEvent::new(MouseEventType::ButtonDown(button)));
                } else {
                    app.handle_event(&MouseEvent::new(MouseEventType::ButtonUp(button)));
                }
            }
            _ => (),
        });
    }
}
