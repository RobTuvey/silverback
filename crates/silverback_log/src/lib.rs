extern crate pretty_env_logger;

pub use log::{debug, error, info, trace, warn};

#[macro_export]
macro_rules! sb_trace {
    ($($arg:tt)*) => {
        trace!($($arg)*);
    }
}

#[macro_export]
macro_rules! sb_debug {
    ($($arg:tt)*) => {
        debug!($($arg)*);
    };
}

#[macro_export]
macro_rules! sb_info {
    ($($arg:tt)*) => {
        info!($($arg)*);
    };
}

#[macro_export]
macro_rules! sb_warn {
    ($($arg:tt)*) => {
        warn!($($arg)*);
    };
}

#[macro_export]
macro_rules! sb_error {
    ($($arg:tt)*) => {
        error!($($arg)*);
    };
}

pub struct LogModule;

impl LogModule {
    pub fn init() {
        pretty_env_logger::init_timed();
    }
}
