use silverback::{App, LogModule};
#[macro_use]
extern crate silverback;

fn main() {
    App::new().run();

    sb_trace!("A trace statement.");
    sb_debug!("A debug statement.");
    sb_info!("An info statement.");
    sb_warn!("A warning statement.");
    sb_error!("An error statement.");
}
