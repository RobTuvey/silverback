use silverback::*;

fn main() {
    App::new().add_module(WindowModule::new()).run();
}
